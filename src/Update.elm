module Update exposing (..)

import Msgs exposing (Msg(..))
import Models exposing (Model, Score)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        EnableEditing score ->
            (toggleEditing model score)

        UpdateScore score newValue ->
            (updateScore model score newValue)

        ChangeSubGame change ->
            case change of
                Models.Inc ->
                    if model.currentSubGame < 4 then
                        ( { model | currentSubGame = model.currentSubGame + 1 }, Cmd.none )
                    else
                        ( model, Cmd.none )

                Models.Dec ->
                    if model.currentSubGame > 1 then
                        ( { model | currentSubGame = model.currentSubGame - 1 }, Cmd.none )
                    else
                        ( model, Cmd.none )


updateScore : Model -> Score -> String -> ( Model, Cmd Msg )
updateScore model score newValue =
    let
        newIntValue =
            Result.withDefault 0 (String.toInt newValue)

        updateScore s =
            if s.id == score.id then
                toggleScoreEdit { s | value = newIntValue }
            else
                s

        updateScores scores =
            List.map updateScore scores

        updatePlayers player =
            if player.id == score.playerId then
                { player | scores = updateScores player.scores }
            else
                player

        updatedPlayers =
            List.map updatePlayers model.players
    in
        ( { model | players = updatedPlayers }, Cmd.none )


toggleEditing : Model -> Score -> ( Model, Cmd Msg )
toggleEditing model score =
    let
        updateScore s =
            if s.id == score.id then
                toggleScoreEdit s
            else
                s

        updateScores scores =
            List.map updateScore scores

        updatePlayers player =
            if player.id == score.playerId then
                { player | scores = updateScores player.scores }
            else
                player

        updatedPlayers =
            List.map updatePlayers model.players
    in
        ( { model | players = updatedPlayers }, Cmd.none )


toggleScoreEdit : Score -> Score
toggleScoreEdit s =
    case s.editing of
        Models.Editing ->
            { s | editing = Models.NotEditing }

        Models.NotEditing ->
            { s | editing = Models.Editing }



{- validate : Model -> Score -> Bool
   validate model score =
       let
           getScore s =
               s.roundId == score.roundId

           getScores player =
               List.filter getScore player.scores

           scores =
               List.map getScores model.players

           flattenedScores =
               Debug.log "scores " (flatten scores)
       in
           case flattenedScores of
               x :: xs ->
                   case x.roundType of
                       _ ->
                           Debug.log "test" (List.foldl scoreCombiner 0 (x :: xs) > 10)

               [] ->
                   False


   scoreCombiner : Score -> Int -> Int
   scoreCombiner s acc =
       acc + s.value


   flatten : List (List a) -> List a
   flatten list =
       List.foldr (++) [] list
-}
