module Msgs exposing (..)

import Models exposing (..)


type Msg
    = NoOp
    | EnableEditing Score
    | UpdateScore Score String
    | ChangeSubGame Change
