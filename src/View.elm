module View exposing (..)

import Html exposing (Html, div, text)
import Msgs exposing (Msg)
import Models exposing (Model)
import Game.Game


view : Model -> Html Msg
view model =
    Game.Game.view model
