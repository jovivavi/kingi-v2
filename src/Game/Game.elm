module Game.Game exposing (..)

import Html exposing (..)
import Html.Events exposing (onClick, on, targetValue)
import Html.Attributes exposing (class, type_, placeholder, tabindex)
import Msgs exposing (Msg(..))
import Models exposing (Model, Player, RoundType, Score, Editing, SubGameId, Change)
import Json.Decode as Json


view : Model -> Html Msg
view game =
    div [ class "container" ]
        [ header []
            [ h1 [] [ text "Kingi" ]
            ]
        , h2 [ class "subgame-header" ] [ text ("Subgame " ++ toString game.currentSubGame) ]
        , listPlayers game.players
        , div [ class "subgame-container" ]
            [ div [ class "arrow", onClick (ChangeSubGame Models.Dec) ] [ text "<" ]
            , viewSubGame game.currentSubGame game.players
            , div [ class "arrow", onClick (ChangeSubGame Models.Inc) ] [ text ">" ]
            ]
        , div [ class "score-line" ] []
        , div [ class "total-score" ] (List.map countGameScore game.players)
        ]


listPlayers : List Player -> Html Msg
listPlayers players =
    div [ class "player-row" ] (List.map playerTag players)


playerTag : Player -> Html Msg
playerTag player =
    div [ class ("player-name" ++ toString player.id) ] [ text player.name ]


viewSubGame : SubGameId -> List Player -> Html Msg
viewSubGame sgid players =
    div [ class "subgame" ]
        [ div [ class "round-list" ] (List.map (renderPlayerRounds sgid) players)
        , div [ class "score-line" ] []
        , div [ class "sub-scores" ] (List.map (countSubGameScore sgid) players)
        ]


countSubGameScore : SubGameId -> Player -> Html Msg
countSubGameScore sgid player =
    let
        filteredScores =
            filterScores sgid player.scores
    in
        div [ class "score" ] [ text (toString (List.foldl scoreCombiner 0 filteredScores)) ]


countGameScore : Player -> Html Msg
countGameScore player =
    div [ class "score" ] [ text (toString (List.foldl scoreCombiner 0 player.scores)) ]


scoreCombiner : Score -> Int -> Int
scoreCombiner s acc =
    if s.editing == Models.Editing then
        acc + 0
    else
        case s.roundType of
            Models.Valtti ->
                acc + s.value

            _ ->
                acc - s.value


renderPlayerRounds : SubGameId -> Player -> Html Msg
renderPlayerRounds sgid player =
    let
        filteredScores =
            filterScores sgid player.scores
    in
        div [ class "player-scores" ] (List.map listScore filteredScores)


filterScores : SubGameId -> List Score -> List Score
filterScores sgid rounds =
    case rounds of
        [] ->
            []

        x :: xs ->
            if x.subGameId == sgid then
                [ x ] ++ filterScores sgid xs
            else
                filterScores sgid xs


listScore : Score -> Html Msg
listScore score =
    case score.editing of
        Models.NotEditing ->
            div
                [ class "score"
                , tabindex (10 * score.id + score.playerId)
                , onClick (EnableEditing score)
                ]
                [ text (toString score.value) ]

        Models.Editing ->
            input
                [ class "score-input"
                , tabindex (10 * score.id + score.playerId)
                , onBlurWithValue (UpdateScore score)
                , placeholder (toString score.value)
                ]
                []



-- https://stackoverflow.com/a/42893807


onBlurWithValue : (String -> Msg) -> Attribute Msg
onBlurWithValue tagger =
    on "blur" (Json.map tagger targetValue)
