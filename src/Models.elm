module Models exposing (..)


type alias Model =
    { players : List Player
    , currentSubGame : SubGameId
    }


initialModel : Model
initialModel =
    { players = initialPlayers
    , currentSubGame = 1
    }


initialPlayers : List Player
initialPlayers =
    [ Player 1 "Pelaaja 1" (initialScores 1 1)
    , Player 2 "Pelaaja 2" (initialScores 2 1)
    , Player 3 "Pelaaja 3" (initialScores 3 1)
    , Player 4 "Pelaaja 4" (initialScores 4 1)
    ]


initialScores : PlayerId -> SubGameId -> List Score
initialScores pid sgid =
    [ initialScore 1 1 pid sgid NoTake
    , initialScore 2 2 pid sgid Hearts
    , initialScore 3 3 pid sgid KnJs
    , initialScore 4 4 pid sgid Queens
    , initialScore 5 5 pid sgid Kingi
    , initialScore 6 6 pid sgid TwoLast
    , initialScore 7 7 pid sgid Valtti
    , initialScore 8 8 pid sgid Valtti
    , initialScore 9 9 pid sgid Valtti
    , initialScore 10 10 pid sgid Valtti
    , initialScore 11 11 pid 2 NoTake
    , initialScore 12 12 pid 2 Hearts
    , initialScore 13 13 pid 2 KnJs
    , initialScore 14 14 pid 2 Queens
    , initialScore 15 15 pid 2 Kingi
    , initialScore 16 16 pid 2 TwoLast
    , initialScore 17 17 pid 2 Valtti
    , initialScore 18 18 pid 2 Valtti
    , initialScore 19 19 pid 2 Valtti
    , initialScore 20 20 pid 2 Valtti
    , initialScore 22 22 pid 3 NoTake
    , initialScore 23 23 pid 3 Hearts
    , initialScore 24 24 pid 3 KnJs
    , initialScore 25 25 pid 3 Queens
    , initialScore 26 26 pid 3 Kingi
    , initialScore 27 27 pid 3 TwoLast
    , initialScore 28 28 pid 3 Valtti
    , initialScore 29 29 pid 3 Valtti
    , initialScore 30 30 pid 3 Valtti
    , initialScore 31 31 pid 3 Valtti
    , initialScore 32 32 pid 4 NoTake
    , initialScore 33 33 pid 4 Hearts
    , initialScore 34 34 pid 4 KnJs
    , initialScore 35 35 pid 4 Queens
    , initialScore 36 36 pid 4 Kingi
    , initialScore 37 37 pid 4 TwoLast
    , initialScore 38 38 pid 4 Valtti
    , initialScore 39 39 pid 4 Valtti
    , initialScore 40 40 pid 4 Valtti
    , initialScore 41 41 pid 4 Valtti
    ]


initialScore : ScoreId -> RoundId -> PlayerId -> SubGameId -> RoundType -> Score
initialScore sid rid pid sgid rType =
    { id = sid
    , roundId = rid
    , playerId = pid
    , subGameId = sgid
    , roundType = rType
    , value = 0
    , editing = Editing
    }


type alias Score =
    { id : ScoreId
    , roundId : RoundId
    , playerId : PlayerId
    , subGameId : SubGameId
    , roundType : RoundType
    , value : Int
    , editing : Editing
    }



-- IDs


type alias SubGameId =
    Int


type alias RoundId =
    Int


type alias PlayerId =
    Int


type alias ScoreId =
    Int



-- Player


type alias Player =
    { id : PlayerId
    , name : String
    , scores : List Score
    }


type RoundType
    = NoTake
    | Hearts
    | KnJs
    | Queens
    | Kingi
    | TwoLast
    | Valtti


type Editing
    = Editing
    | NotEditing


type Change
    = Inc
    | Dec
